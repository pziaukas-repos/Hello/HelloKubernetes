# Hello Kubernetes

Kubernetes demo project, based on
* [Docker](https://www.docker.com)
* [Minikube](https://github.com/kubernetes/minikube)



## Docker

#### Use the container locally
Build the demo container:

`docker build k8s-hello --tag hello:latest`

Run the container:

`docker run -p 3000:3000 -it hello:latest`

#### Push to the Docker Hub
Login to the service:

`docker login`

Tag the local image:

`docker tag hello:latest pranasziaukas/hello`

Push to the hub:

`docker push pranasziaukas/hello`



## Kubernetes

### Basic commands

#### Usage
Start a cluster by running:

`minikube start`

Access the Kubernetes Dashboard running within the minikube cluster:

`minikube dashboard`

Stop your local cluster:

`minikube stop`

#### Extras
Get information about all running pods:

`kubectl get pod`

Describe one pod:

`kubectl describe pod <pod>` 

Expose the port of a pod (creates a new service):

`kubectl expose pod <pod> --port=444 --name=frontend`

Port forward the exposed pod port to your local machine:

`kubectl port-forward <pod> 8080`

Attach to the pod:

`kubectl attach <podname> -i`

Execute a command on the pod:

`kubectl exec <pod> -- command`

Add a new label to a pod:

`kubectl label pods <pod> mylabel=awesome`

Run a shell in a pod - very useful for debugging:

`kubectl run -i --tty busybox --image=busybox --restart=Never -- sh`


### Working with images

#### Example image
Once started, you can interact with your cluster using `kubectl`, just like any other Kubernetes cluster. For instance, starting a server:

`kubectl create deployment hello-minikube --image=k8s.gcr.io/echoserver:1.4`

Exposing a service as a NodePort:

`kubectl expose deployment hello-minikube --type=NodePort --port=8080`

Minikube makes it easy to open this exposed endpoint in your browser:

`minikube service hello-minikube`

#### Personal image
Define a Kubernetes pod consisting of a single image, `k8s-definitions/helloworld-pod.yml`.

Create the pod on the cluster:

`kubectl create -f k8s-definitions/helloworld-pod.yml`

Check the events going on inside the pod:

`kubectl describe pod helloworld.example.com`

Forward to the port `8081`on the local machine:

`kubectl port-forward helloworld.example.com 8081:3000`

Alternatively, create a service:

`kubectl expose pod helloworld.example.com --type=NodePort --name=helloworld-service`

One would create a `.yml` file for AWS to define a service (and, for example, put the pod under a load balancer).
For Minikube it suffices to execute an `expose` command.

Finally, retrieve its URL:

`minikube service helloworld-service --url`


### Replications

#### Replication controller
Stateless applications can be scaled horizontally by defining a `ReplicationController`
such as `k8s-definitions/helloworld-repl-controller.yml`, and executing:

`kubectl create -f k8s-definitions/helloworld-repl-controller.yml`

Replication controlled pods are resilient to failures and termination.
If you delete a pod that was just launched:

`kubectl delete pod helloworld-controller-<suffix>`

A new instance is created automatically.
Moreover, you can scale the replicas manually as well:

`kubectl scale --replicas=4 -f k8s-definitions/helloworld-repl-controller.yml`

#### Replication set
The replication set allows for a precise filtering according to the set of values
compared to the replication controller that essentially works with a value equality.


### Deployments

Deployment objects allow you to do application deployments and updates:
- Creations
- Updates
- Rolling updates
- Rollbacks

Deployments can be seen as a more flexible and multi-functional alternative compared to straight replications. 

#### Usage
Applications can be deployed by defining a `Deployment`
such as `k8s-definitions/helloworld-deployment.yml`, and executing:

`kubectl create -f k8s-definitions/helloworld-deployment.yml`

#### Extras
Get information on current deployments:

`kubectl get deployments`

Get information about the replica sets:

`kubectl get rs`

Get pods, and also show labels attached to those pods:

`kubectl get pods --show-labels`

Get deployment status:

`kubectl rollout status deployment/helloworld-deployment`

Get the status of the rollout:

`kubectl rollout status deployment/helloworld-deployment`

Rollback to previous version:

`kubectl rollout undo deployment/helloworld-deployment`

### Services

Define the service in `k8s-definitions/helloworld-service.yml`, and execute:

`kubectl create -f k8s-definitions/helloworld-service.yml`

It is now a service (a logical bridge) that opens access to the pods instead of accessing them directly,
plus provides load balancing.

Get the status:

`kubectl describe svc helloworld-service`
 